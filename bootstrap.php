<?php
/**
 * The Bootstrap file
 *
 * It is to sets up the environment and configuration for the application
 *
 * LICENSE:
 *
 * @copyright  2012 Monara IT UK Ltd
 * @license    http://www.pluspro.com/license/3_0.txt   PHP License 3.0
 * @version    PlusPro V4
 * @since      File available since Release 1.0.0
 */
require_once 'configs/config.ini.php';	

session_start();

// Application Error settings....
error_reporting(E_ALL);
if($_DISPLAY_ERRORS == true){
	ini_set('display_errors','On');
} else {
	ini_set('display_errors','Off');
}

// the document root path...
define('DOC_ROOT', $_DOC_ROOT);

// the php class library path...
$library_path       = DOC_ROOT."/application/classes/";
define('LIBRARY_PATH', $library_path);

// the database server settings...
define('DB_SERVER', $_DB_SERVER);
define('DB_USER', $_DB_USER);
define('DB_PASSWORD', $_DB_PASSWORD);
define('DB_NAME', $_DB_NAME);

require_once 'application/auto_loader.php';
require_once 'application/classes/Misc.php';

// the admin js libs path
define('JS_LIBS_PATH', $_JS_LIBS_PATH);

// the global contents path
define('GLOBAL_ADMIN_CONTENT_PATH', $_GLOBAL_ADMIN_CONTENT_PATH);

/************* GET SITE SETTINGS *******/
$settingId = 1;
$objSiteSetting         = new Setting();
$siteSettings           = $objSiteSetting->getSetting($settingId);
$siteId                 = $siteSettings->id;
$siteAdminBaseUrl       = $siteSettings->url;
$siteName               = $siteSettings->siteName;
$siteEmail              = $siteSettings->siteEmail;
$siteTemplate           = $siteSettings->template;
$siteSideBox            = $siteSettings->sideBox;
$siteBaseUrl            = $siteSettings->baseUrl;
$siteNeedPing           = $siteSettings->needPing;
$siteMaxNumPages        = $siteSettings->max_num_pages;
$plusProAltText         = $siteSettings->pluspro_logo_text;
$siteLogo               = $siteSettings->logo;
$siteFav                = $siteSettings->favIcon;
$siteHomeBannerH        = $siteSettings->homeBannerHeight;
$siteHomeBannerW        = $siteSettings->homeBannerWidth;
$is_mobile_site_enabled = $siteSettings->mobileSite;
/**************************************/

// the admin base url and template path
$_ADMIN_LAYOUT_PATH = DOC_ROOT.'/admin/templates/'; // admin template path
define('ADMIN_LAYOUT_PATH', $_ADMIN_LAYOUT_PATH);

$_SUPER_ADMIN_LAYOUT_PATH = DOC_ROOT.'/admin/sadmin/templates/'; // admin template path
define('SUPER_ADMIN_LAYOUT_PATH', $_SUPER_ADMIN_LAYOUT_PATH);

define('ADMIN_BASE_URL', $siteAdminBaseUrl);
define('SITE_NAME', $siteName);
define('SITE_EMAIL', $siteEmail);
define('SITE_TEMPLATE', $siteTemplate);
define('SITE_SIDE_BOX', $siteSideBox);
define('SITE_BASE_URL', $siteBaseUrl);
define('SITE_NEEED_PING', $siteNeedPing);
define('SITE_MAX_PAGES', $siteMaxNumPages);
define('SITE_PLUSPRO_ALT_TEXT', $plusProAltText);
define('SITE_LOGO', $siteLogo);
define('SITE_FAV_ICON', $siteFav);
define('SITE_HOMEBANNER_HEIGHT',$siteHomeBannerH);
define('SITE_HOMEBANNER_WIDTH',$siteHomeBannerW);
define('IS_MOBILE_SITE_ENABLE',$is_mobile_site_enabled);


/************* GET SITE CONTACT DETAILS *******/
$siteContentId = 1;
$objSiteContact = new SiteContact();
$siteContactInfo = $objSiteContact->getSiteContact($siteContentId);

$siteContactId          = $siteContactInfo->id;
$siteContactName        = $siteContactInfo->contact_name;
$siteContactEmail       = $siteContactInfo->address_line_1;
$siteAddressLine_1      = $siteContactInfo->address_line_2;
$siteAddressLine_2      = $siteContactInfo->address_line_3;
$siteContactNum_1       = $siteContactInfo->contact_number_1;
$siteContactNum_2       = $siteContactInfo->contact_number_2;
$siteGoogleMapCode      = $siteContactInfo->google_map_code;
$new_number2      = $siteContactInfo->new_number2;
$new_email      = $siteContactInfo->new_email;
$info_email      = $siteContactInfo->info_email;
/**************************************/

define('SITE_CONTACT_NAME',$siteContactName);
define('SITE_CONTACT_EMAIL',$siteContactEmail);
define('SITE_CONTACT_ADDR_1',$siteAddressLine_1);
define('SITE_CONTACT_ADDR_2',$siteAddressLine_2);
define('SITE_CONTACT_NUM_1',$siteContactNum_1);
define('SITE_CONTACT_NUM_2',$siteContactNum_2);
define('SITE_GMAP_CODE',$siteGoogleMapCode);
define('NEW_NUMBERS',$new_number2);
define('NEW_EMAIL',$new_email);
define('INFO_EMAIL',$info_email);
/************ SOCIAL MEDIA SETTINGS **********/
$sSocialMediaSettingId              = 1;
$objSocialMediaSetting              = new SocialMediaSetting();
$siteSocialMediaSettingInfo         = $objSocialMediaSetting->getSocialMediaSetting($sSocialMediaSettingId);

$siteId                             =   $siteSocialMediaSettingInfo->id;
$siteFacebook                       =   $siteSocialMediaSettingInfo->facebook;
$siteTwitter                        =   $siteSocialMediaSettingInfo->twitter;
$siteLinkedIn                       =   $siteSocialMediaSettingInfo->linkedIn;
$siteGooglePlus                     =   $siteSocialMediaSettingInfo->googlePlus;
$siteOtherSocialMLinks              =   $siteSocialMediaSettingInfo->other;

define('SITE_FACEBOOK',$siteFacebook);
define('SITE_TWITTER',$siteTwitter);
define('SITE_LINKEDIN',$siteLinkedIn);
define('SITE_GOOGLEPLUS',$siteGooglePlus);
define('SITE_OTHERSOCIALLINKS',$siteOtherSocialMLinks);

/*********************************************/

// the modules templates path....
$_MODULE_TEMPLATES_PATH = 'templates/'; // admin template path
define('MODULE_TEMPLATES_PATH', $_MODULE_TEMPLATES_PATH);

// the front end layout settings...
// the front layout path...
$front_layout_path = DOC_ROOT."/includes/templates/".SITE_TEMPLATE."/layouts/";//$template;
define('FRONT_LAYOUT_PATH', $front_layout_path);

// the front layout view path...
$front_layout_view_path = DOC_ROOT."/includes/templates/".SITE_TEMPLATE."/views/"; // the view page path
define('FRONT_LAYOUT_VIEW_PATH', $front_layout_view_path);

$front_template_http_url = SITE_BASE_URL."includes/templates/".SITE_TEMPLATE."/";//$template;
define('FRONT_LAYOUT_URL', $front_template_http_url);

// the lhs_side_bar......
$is_sidebox_enabled = "";
if ($siteSideBox == '1' || $siteSideBox == '2' ) {
	$is_sidebox_enabled = "Yes";
} else {
	$is_sidebox_enabled = "No";
}
define('IS_SIDEBOX_ENABLED', $is_sidebox_enabled);

// the pagination data settings
define('ADMIN_RECORDS_PER_PAGE', $_ADMIN_RECORDS_PER_PAGE);
define('FRONT_RECORDS_PER_PAGE', $_FRONT_RECORDS_PER_PAGE);
?>